//
//  HomeScreenSteps.swift
//  ReferenceiOSUITests
//
//  Created by Owner on 26/12/2021.
//  Copyright © 2021 ABN AMRO. All rights reserved.
//

import Foundation
import XCTest
import Cucumberish

class HomeScreenSteps: NSObject {
    
    func HomeScreenSteps(_ application: XCUIApplication) {
        let homeScreen = HomeScreen(application: application)
        
        Then("I should see text \"([^\\\"]*)\" in home screen"){(args, userInfo) -> Void in
            let expectedText = (args?[0])!
            let actualText = homeScreen.textLabel.label
            XCTAssertEqual(expectedText, actualText,"Text is not displayed in home screen")
        }
        
        Then("I should see button \"([^\\\"]*)\" in home screen") {(args, userInfo) -> Void in
            XCTAssertTrue(homeScreen.button.exists, "Button is not displayed in home screen")
            XCTAssertEqual((args?[0])!, homeScreen.button.label,"Button text is not same as expected")
        }
        
        When("I tap the button on the screen") { (args, userInfo) -> Void in
            homeScreen.clickButton()
        }
        
        Then("I should see euro \"([^\\\"]*)\" symbol in currency value displayed"){(args, userInfo) -> Void in
            let expectedSymbol = (args?[0])!
            XCTAssertTrue(homeScreen.currency.label.contains(expectedSymbol), "Euro € symbol is not available")
        }
        Then("I should see currency is between the range of \"([^\\\"]*)\" to \"([^\\\"]*)\""){ [self](args, userInfo) -> Void in
            let minValue = (args?[0])!
            let maxValue = (args?[1])!
            assertionCheckForRange(currency: homeScreen.currency.label, minValue: minValue, maxValue: maxValue)
        }
        When("I tap the button on the screen for \"([^\\\"]*)\" times and is within range of \"([^\\\"]*)\" to \"([^\\\"]*)\""){ [self](args, userInfo) -> Void in
            let range = 1...Int((args?[0])!)!
            for _ in range {
                homeScreen.clickButton()
                assertionCheckForRange(currency: homeScreen.currency.label, minValue: (args?[1])!, maxValue: (args?[2])!)
                XCTAssertTrue(homeScreen.currency.label.contains("€"), "Euro € symbol is not available")
            }
        }
        
    }
    
    func removeACharacter(currency : String) -> String{
        var value = currency
        let removeCharacters: Set<Character> = ["€"]
        value.removeAll(where: { removeCharacters.contains($0) })
        return value
    }
    
    func currencyToDecimalConverter(currency: String) -> Decimal{
        var value = currency
        value = removeACharacter(currency: value)
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        formatter.locale = Locale(identifier: "de_DE")
        let number = formatter.number(from: value)
        
        return number!.decimalValue
    }
    
    func assertionCheckForRange(currency: String,minValue: String,maxValue: String) -> Void {
        XCTAssertLessThanOrEqual(currencyToDecimalConverter(currency: currency),currencyToDecimalConverter(currency:maxValue) ,"Currency is not less than 99999999")
        XCTAssertGreaterThanOrEqual(currencyToDecimalConverter(currency: currency), currencyToDecimalConverter(currency: minValue),"Currency is not greater than 100")
    }
    
}
