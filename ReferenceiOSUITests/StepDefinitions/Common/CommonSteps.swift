//
//  CommonSteps.swift
//  ReferenceiOSUITests
//
//  Created by Owner on 26/12/2021.
//  Copyright © 2021 ABN AMRO. All rights reserved.
//

import XCTest
import Cucumberish

class CommonStepDefinitions: NSObject {
    fileprivate var application : XCUIApplication!
    
    fileprivate func setup(_ application: XCUIApplication)
        {
            self.application = application
            Given("I launched the App") { (args, userInfo) in
                application.launch()
             }
            Then("I terminate the App") { (args, userInfo) in
                application.terminate()
            }
        }
    class func setup(_ application: XCUIApplication)
        {
            CommonStepDefinitions().setup(application)
        }
}
