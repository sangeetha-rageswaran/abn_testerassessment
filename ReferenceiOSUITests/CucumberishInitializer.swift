//
//  CucumberishInitializer.swift
//  ReferenceiOSUITests
//
//  Created by Owner on 26/12/2021.
//  Copyright © 2021 ABN AMRO. All rights reserved.
//
import XCTest
import Foundation
import Cucumberish

class CucumberishInitializer: NSObject{

@objc class func setupCucumberish()
{
    var application : XCUIApplication!
    //A closure that will be executed just before executing any of your features
    before({ _ in
        application = XCUIApplication()
        CommonStepDefinitions.setup(application);
        HomeScreenSteps().HomeScreenSteps(application)
        
    })
 
    let bundle = Bundle(for: CucumberishInitializer.self)

    Cucumberish.executeFeatures(inDirectory: "Features", from: bundle, includeTags: self.getTags(), excludeTags: nil)
}
    fileprivate class func getTags() -> [String]? {
            var itemsTags: [String]? = nil
            for i in ProcessInfo.processInfo.arguments {
                if i.hasPrefix("-Tags:") {
                    let newItems = i.replacingOccurrences(of: "-Tags:", with: "")
                    itemsTags = newItems.components(separatedBy: ",")
                }
            }
            return itemsTags
        }
}
