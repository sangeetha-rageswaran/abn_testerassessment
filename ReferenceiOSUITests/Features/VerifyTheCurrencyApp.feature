Feature: Launch the app and check for currency value

Background:
    Given I launched the App
    
@ios.sit.test
Scenario: Launch the app and Verify the homescreen
    Then I should see text "Hello" in home screen
    Then I should see button "Button" in home screen
    Then I terminate the App
     
@ios.sit.test
Scenario: Click the button in home screen and verify the currency
    When I tap the button on the screen
    Then I should see euro "€" symbol in currency value displayed
    Then I should see currency is between the range of "100" to "99999999"
    Then I terminate the App

@ios.system.test
Scenario: Click the button in home screen more than once
    When I tap the button on the screen for "5" times and is within range of "100" to "99999999"
    Then I terminate the App
