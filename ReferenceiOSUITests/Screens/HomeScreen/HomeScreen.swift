//
//  HomeScreen.swift
//  ReferenceiOSUITests
//
//  Created by Owner on 27/12/2021.
//  Copyright © 2021 ABN AMRO. All rights reserved.
//

import XCTest
import Foundation

class HomeScreen {
    
    let application: XCUIApplication
    let textLabel: XCUIElement
    let button: XCUIElement
    let currency: XCUIElement
    
    init(application: XCUIApplication){
        self.application = application
        textLabel = application.staticTexts["label"]
        button = application.buttons["Button"]
        currency = application.staticTexts["label"]
    }
    
    func clickButton(){
        button.tap()
    }
    
}
