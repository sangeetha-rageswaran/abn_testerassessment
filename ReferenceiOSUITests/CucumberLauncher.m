//
//  CucumberLauncher.m
//  ReferenceiOSUITests
//
//  Created by Owner on 26/12/2021.
//  Copyright © 2021 ABN AMRO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ReferenceiOSUITests-Swift.h"

__attribute__((constructor))
void CucumberishInit(){
    [CucumberishInitializer setupCucumberish];
}
