# ABN_testerassessment



## **Installation**

### **Local Installations**

#### **Install Xcode**

- ​	Use appstore to install Xcode in you MAC machine (It can take a while to complete the installation)

#### **Install git**

https://git-scm.com/downloads

#### **Setup XCUITest with Cucumberish**

1. Download the assesment from [googleDrive](https://eur03.safelinks.protection.outlook.com/?url=https%3A%2F%2Fdrive.google.com%2Ffile%2Fd%2F1KkuZNHuPvaH_8VZ_QSldx1Iv7BUxTWy4%2Fview%3Fusp%3Dsharing&data=04%7C01%7C%7Cc5a37355252a42ae4bbe08d9a069d1a2%7C3a15904d3fd94256a753beb05cdf0c6d%7C0%7C0%7C637717198614813706%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&sdata=KQSqn3ij8Q5cPCoQO4XjFVSVjDwL7K4SHxpm9HBqej4%3D&reserved=0)
2. Open the zip file
3. Install cocoapods using 
   - `brew install cocoapods``
   - ``brew link --overwrite cocoapods` -> use this if its not linked properly
4. change the directory to the assignment folder
   - `pod init` ---> This will generate a podFile 
   - `open Podfile` ---> Opens the Podfile
   - Add `pod 'Cucumberish'` under the target where you want to write your cucumber tests. Here in our case its `ReferenceiOSUITests`
   - Save and close the file. Then do `pod install` , this will initialise the pod. Now we are ready to setup our cucumber project.
5. Now, double click on `ReferenceiOS.xcworkspace` from the project root this will open the project using Xcode, need to do some pre-steps before start writing the code. Those are as follows:
   - Choose the target scheme as  `ReferenceiOSUITests`
   - Go to Build Settings, search for module under Packing enable the Defines module to **Yes** 
   - Also Go to Edit scheme and confirm your target has shared checkbox clicked. This is needed to run the test using CLI and from Gitlab
6. Now you can start create your cuccumberish/BDD tests. This will be explained in the project structure section.

#### **Project Structure**

**ReferenceiOSUITests**

- CucumberishInitializer ---> File that holds the place where to look for feature files and if needed to use tags for execution or not.
  											Also loads the StepDefinition classes before executing the feature files
- CucumberLauncher ---> This helps to launch the CucumberishInitializer
- StepDefinitions
  - HomeScreen ---> Here we can find the stepDefinition for homeScreen implementation
  - CommonSteps ---> Currently this holds the common step definitions for launching and terminate the app. (This can also be replaced by override func, this is added now for structural view)
- Features ---> This contains the feature file written using the Gherkin Language (Brief overview)
  - Given : conditions to be satisfied before applying any action
  - When : action statement
  - Then : Observe the outcomes
  - Scenario : Is similar to our test cases. Each scenario has a title that describes what's done in that scenario and steps that perform the test
  - Background: Is something like precondition which will be execute before all the scenarios (In order to use this I have created the step definition to launch the app)
  - Tags: I have used 2 tags in this case @ios.system.test and @ios.sit.test, so basically using this you can specify which tests to be execute and which not.
- Screens
  - HomeScreen ---> has the locators 

.gitlab-ci.yml -->  *root path* ---> Pipeline script to run the tests from Gitlab

#### **Fastlane File**

​	Updated the workspace, scheme, path of output, devices to use under Run UI tests in  Fastlane file. Using this, tests can be triggered from terminal in local machine and also from Gitlab.





